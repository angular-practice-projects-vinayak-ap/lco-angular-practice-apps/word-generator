import { Component } from '@angular/core';
import { arrayWords, arrayCountries } from '../utilities/arrays';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'word-generator';

  entities = '';
  wordsLimit = 10;
  countriesLimit = 5;

  handleSlideChange(newLimit: number) {
    this.wordsLimit = newLimit;
  }

  generateWords() {
    this.entities = arrayWords.slice(0, this.wordsLimit).join(' ');
  }

  generateCountryList() {
    this.countriesLimit = Math.floor(Math.random() * 24) + 1;
    this.entities = arrayCountries.slice(0, this.countriesLimit).join(' ');
  }
}
